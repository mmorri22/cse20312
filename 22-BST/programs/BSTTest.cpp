#include "../classes/BST.h"

#include <iostream>
#include <string>

template<class T>
void insertVal( BST<T>& theTree, const T& theValue ){
	
	std::cout << "Inserting " << theValue << "... ";
	theTree.Insert(theValue);
	theTree.printInOrder( std::cout );	
	
}


template<class T>
void removeVal( BST<T>& theTree, const T& theValue ){
	
	std::cout << "Removing " << theValue << "... ";
	theTree.remove(theValue);
	theTree.printInOrder( std::cout );	
	
}

void testIntTree(){
	
	BST<int> theTree;
	
	insertVal(theTree, 22);
	insertVal(theTree, 44);
	insertVal(theTree, 18);	
	insertVal(theTree, 17);
	insertVal(theTree, 35);
	insertVal(theTree, 21);	
	insertVal(theTree, 22);	
	insertVal(theTree, 45);
	
	removeVal(theTree, 22);	
	removeVal(theTree, 18);	
	removeVal(theTree, 21);		
	removeVal(theTree, 35);
	removeVal(theTree, 44);	
	removeVal(theTree, 17);
	removeVal(theTree, 45);		
	
}

void testStrTree(){
	
	BST<std::string> theTree;
	
	insertVal(theTree, std::string("Wake"));
	insertVal(theTree, std::string("up"));
	insertVal(theTree, std::string("the"));	
	insertVal(theTree, std::string("echoes"));
	insertVal(theTree, std::string("cheering"));
	insertVal(theTree, std::string("her"));	
	insertVal(theTree, std::string("name"));	
	
	removeVal(theTree, std::string("Wake"));
	removeVal(theTree, std::string("up"));
	removeVal(theTree, std::string("the"));	
	removeVal(theTree, std::string("echoes"));
	removeVal(theTree, std::string("cheering"));
	removeVal(theTree, std::string("her"));	
	removeVal(theTree, std::string("name"));		
	
}

int main(){
	
	testIntTree();
	std::cout << "--------------------" << std::endl;
	testStrTree();
	
	return 0;
}