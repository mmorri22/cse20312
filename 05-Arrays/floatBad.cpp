/**********************************************
* File: floatBad.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows a fundamental example of floats  
* This code fails because of implicit type conversion 
**********************************************/

#include <iostream>
#include <iomanip>	/* Included for std::setprecision */
#include <cmath>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Variable declarations */
	float slideFloat = 3.2;
	float smallFloat = -1.0125 * pow(2, -127);
	float largeFloat = pow(2, 127);

	return 0;
}



