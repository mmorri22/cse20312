/**********************************************
* File: double.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows a fundamental example of double  
**********************************************/

#include <iostream>
#include <iomanip>	/* Included for std::setprecision */
#include <cmath>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	/* No issue, since the default cast for the device is double */
	double smallDouble = -1.0125 * pow(2, -305);
	double slideDouble = 0.15625;
	double largeDouble = 1.8725 * pow(2, 305);
	
	std::cout << "Decimal Representation of smallDouble is: " << smallDouble << std::endl;
	
	std::cout << "Decimal Representation of slideDouble is: " << slideDouble << std::endl;
		
	std::cout << "Decimal Representation of largeDouble is: " << largeDouble << std::endl;

	return 0;
}
