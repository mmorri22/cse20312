/**********************************************
* File: charDupes.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains solutions to Part A and Part B
* of the Week 3 Creativity Challenge
*
* NOTE: This is a Spiral 1 example for CSE 20312 Data
* Structures students, and contains deliberate compiler 
* errors used for course instruction 
**********************************************/
#include <iostream>			// Output Stream
#include <string>			// String
#include <unordered_map>		// Hash Table
#include <cstdlib>			// malloc


/********************************************
* Function Name  : repeatHashCheck
* Pre-conditions : char* charArr, long unsigned int strLen
* Post-conditions: bool
* 
* This function checks the char* to see if it contains any
* duplicate chars. If all are unique, returns true.
* Otherwise, returns false 
********************************************/
bool repeatHashCheck(char* charArr, long unsigned int strLen){
	
	// Create a Hash Table
	std::unordered_map<bool, char> charCount;
	
	for(long unsigned int iter = 0; iter < strLen; iter++){
		
			if(charCount.count(charArr[iter]) != 0){
				
				// Repeated Character!
				std::cout << charArr << " has a repeating char\n";
				return false;
			}
			
			charCount.insert( {true, charArr[iter]} );
		
	}
	
	std::cout << charArr << " has all unique chars\n";
	return true;
	
}


/********************************************
* Function Name  : Merge
* Pre-conditions : char* charArr, long unsigned int left, long unsigned int middle, long unsigned int right
* Post-conditions: none
* 
* Contains the Merge function for MergeSort 
********************************************/
void Merge(char* charArr, long unsigned int left, long unsigned int middle, long unsigned int right) 
{ 
    long unsigned i, j, k; 
    long unsigned n1 = middle - left + 1; 
    long unsigned n2 =  right - middle; 
  
    /* create temp arrays */
    char* Left = new char[n1]; 
	char* Right = new char[n2]; 
  
    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++) 
        Left[i] = charArr[left + i]; 
    for (j = 0; j < n2; j++) 
        Right[j] = charArr[middle + 1+ j]; 
  
    /* Merge the temp arrays back into arr[l..r]*/
    i = 0; // Initial index of first subarray 
    j = 0; // Initial index of second subarray 
    k = left; // Initial index of merged subarray 
    while (i < n1 && j < n2) 
    { 
        if (Left[i] <= Right[j]) 
        { 
            charArr[k] = Left[i]; 
            i++; 
        } 
        else
        { 
            charArr[k] = Right[j]; 
            j++; 
        } 
        k++; 
    } 
  
    /* Copy the remaining elements of L[], if there 
       are any */
    while (i < n1) 
    { 
        charArr[k] = Left[i]; 
        i++; 
        k++; 
    } 
  
    /* Copy the remaining elements of R[], if there 
       are any */
    while (j < n2) 
    { 
        charArr[k] = Right[j]; 
        j++; 
        k++; 
    } 
	
	delete Left;
	delete Right;
} 


/********************************************
* Function Name  : MergeSort
* Pre-conditions : char* charArr, long unsigned int left, long unsigned int right
* Post-conditions: none
* 
* This method runs the recursive MergeSort algorithm on a char array
* NOTE: Contains a DELIBERATE Compiler Error! 
********************************************/
void MergeSort(char* charArr, long unsigned int left, long unsigned int right){
	
	/* Failure to check left==right first will result in a segmentation fault */
	
	if(right > 1){
		
		long unsigned int middle = (left + right)/2;
		MergeSort(charArr, left, middle);
		MergeSort(charArr, middle+1, right);
		Merge(charArr, left, middle, right);
		
	}
	
}


/********************************************
* Function Name  : MergeSort
* Pre-conditions : char* charArr, long unsigned int strLen
* Post-conditions: none
* 
* This method is the initial call to MergeSort. Not recursive. 
********************************************/
void MergeSort(char* charArr, long unsigned int strLen){
	
	MergeSort(charArr, 0, strLen-1);
	
}



/********************************************
* Function Name  : repeatIterCheck
* Pre-conditions : char* charArr, long unsigned int strLen
* Post-conditions: none
* 
* Iterates through a sorted array and checks if it contains
* any duplicates.
*
* NOTE: Contains a deliberate logic error 
********************************************/
void repeatIterCheck(char* charArr, long unsigned int strLen){
	
	/* This will call charArr[strLen] == charArr[strLen+1] */
	/* Both are null, so the result will return false for every string */
	for(long unsigned int iter = 0; iter <= strLen; iter++){
		
		if(charArr[iter] == charArr[iter+1]){
			
			std::cout << charArr << " is not unique\n";
			return;
			
		}
		
	}
	
	std::cout << charArr << " is unique!" << std::endl;
}


/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(){
	
	// Allocate Memory
	long unsigned int strLen = 6;
	char* uniqueArr = (char *)malloc(strLen * sizeof(char));
	char* repeatArr = (char *)malloc(strLen * sizeof(char));	
	
	// All characters must be allocated individually
	uniqueArr[0] = 'a';
	*(uniqueArr + 1) = 'e';
	uniqueArr[2] = 'i';
	*(uniqueArr + 3) = 'o';
	uniqueArr[4] = 'u';
	*(uniqueArr + 5) = 'y';
	
	repeatArr[0] = 'a';
	*(repeatArr + 1) = 'e';
	repeatArr[2] = 'i';
	*(repeatArr + 3) = 'a';		// Repeat
	repeatArr[4] = 'u';
	*(repeatArr + 5) = 'y';
	
	// Run the Part A Solution
	std::cout << "Part A Solutions" << (char)10;
	repeatHashCheck(uniqueArr, strLen);
	repeatHashCheck(repeatArr, strLen);

	// Run the Part B Solution
	std::cout << "\nPart B Solutions" << std::endl;	
	
	// First, sort the arrays
	MergeSort(uniqueArr, strLen);
	std::cout << "Sorted Array 1: " << uniqueArr << std::endl;
	MergeSort(repeatArr, strLen);
	std::cout << "Sorted Array 2: " << repeatArr << std::endl;
	
	// Then, check by iteration
	repeatIterCheck(uniqueArr, strLen);
	repeatIterCheck(repeatArr, strLen);
	
	// Free the memory
	free(uniqueArr);
	free(repeatArr);
}
