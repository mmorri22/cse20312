#include <iostream>

int recurFunc2(int* intArr, int a, int b){

    if(a == b){
        return intArr[a];
    }
        
    else if(b - a == 1){
        if( intArr[a] > intArr[b] ){
            return intArr[a];
        }
        return intArr[b];
    }

    int c = (a + b) / 2;

    int d = recurFunc2( intArr, a, c );
    int e = recurFunc2( intArr, c+1, b );

    if( d > e )
        return d;

    return e;
}

int main(void){
	
	long unsigned int arrLen = 5;

    int* intArr = new int[arrLen];
    *(intArr + 0) = 13;
    intArr[1] = 22;
    intArr[2] = 9;
    *(intArr + 3) = -7;
    intArr[4] = 10;

    std::cout << recurFunc2( intArr, 0, (int)arrLen-1 ) << std::endl;

    return 0;
}