/**********************************************
* File: fact.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf

#include "factFunc.h"

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
* Main driver for factorial fundamental
* Requires a valid positive integer as argv[1]
********************************************/
int main(int argc, char** argv){

	if(argc != 2){
		std::cout << "Incorrect number of inputs" << std::endl;
		exit(-1);
	}
	
	std::string strUnsigX;
	unsigned int unsigX;
	
	/* Check if a valid unsigned integer */
	getValue(argv[1], unsigX);

	// Call the recursive function
	std::cout << "Recursive Final Result: " << factorial(unsigX) << std::endl;

	return 0;

}
