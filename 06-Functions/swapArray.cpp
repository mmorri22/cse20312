/**********************************************
* File: swapArray.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file combines concepts for students by 
* swapping values in an array with pass by reference
**********************************************/

#include <iostream>
#include <cstdlib> // Included for malloc

/********************************************
* Function Name  : swapInts
* Pre-conditions : int* word, long unsigned int numInts, int a, int b
* Post-conditions: none
* 
* Swaps the values of a and b in the array 
********************************************/
void swapInts(int* word, long unsigned int numInts, int a, int b){
	
	/* Error Checking */
	if( a < 0 || b < 0 ){
		std::cout << "Invalid input to swapInts. Less than 0" << std::endl;
		return;
	}
	
	if( a >= (int)numInts || b >= (int)numInts ){
		std::cout << "Invalid input to swapInts. Exceeds the array length" << std::endl;;
		return;
	}
	
	/* Swap valid array locations */
	std::cout << "Before: word[" << a << "] = " << word[a] << " and word[" << b << "] = " << word[b] << "\n";
	
	/* Swap */
	int temp = word[a];
	*(word + a) = word[b];
	word[b] = temp;
	
	std::cout << "After: word[" << a << "] = " << word[a] << " and word[" << b << "] = " << word[b] << "\n";
	
}

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Declare variables */
	long unsigned int numInts = 6;
	int* swapInt = (int *)malloc(numInts * sizeof(int));
	
	/* Store the values */
	int i;
	for(i = 0; i < (int)numInts; i++){
		
		*(swapInt + i) = i*2;	// Equivalent to swapInt[i] = i*2;
		
	}
	
	swapInts(swapInt, numInts, 0, i-1);
	std::cout << std::endl;
	swapInts(swapInt, numInts, -1, i-1);
	std::cout << std::endl;
	swapInts(swapInt, numInts, 0, (int)numInts);
	std::cout << std::endl;
	swapInts(swapInt, numInts, 1, 3);	
	
	/* Free Memory */
	free(swapInt);

	return 0;
}

