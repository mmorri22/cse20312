/**********************************************
* File: func.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* A simple example of functions in the computing device 
**********************************************/

#include <iostream>
int total; /* Global Variable */

int Square(int x){
	x = x*x;
	return x;
}

int SumOfSquare(int x, int y){
	int z = Square(x + y);
	return z;
}

int main(void){
	int x = 3, y = 4;
	total = SumOfSquare(x, y);
	std::cout << "The square of the sum of " << x << " and " 
		<< y << " is " << total << std::endl;
		
	return 0;
	
}
