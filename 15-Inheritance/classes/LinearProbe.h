#ifndef LINEARPROBE_H
#define LINEARPROBE_H

#include <vector>
#include <iostream>
#include "HashFunc.h"
#include "Primes.h"

enum EntryState { ACTIVE, EMPTY, DELETED };

const int STEPSIZE = 1;
const int COLLISION_RATE = 2;

template<class Key, class Value>
class HashTable{
	
	protected:
	
		struct HashEntry{
			
			Value element;
			EntryState state;
			
			HashEntry() : element(), state( EMPTY ) {}
			
			HashEntry( const Value& theValue, EntryState i = EMPTY ) :
				element(theValue), state(i) {}
			
		};
		
		unsigned int numHash;			// # of Elements Hashed
		std::vector<HashEntry> array;	// Dynamic Array containing Hash Entries
		
		bool isActive( unsigned int currentPos ) const {
			
			return array[ currentPos ].state == ACTIVE;
			
		}
		
		
		long unsigned int capacity() const{
			
			return array.capacity();
		}
		
		
		virtual unsigned int findPos(Value theValue) const{
			
			unsigned int currentPos;
			unsigned int iter = 0;
			
			do{
				currentPos = (HashFunc(theValue) + iter*STEPSIZE) % (unsigned int)capacity();
				++iter;
			}
			while(
				array[ currentPos ].state != EMPTY
				&& array[ currentPos ].state != DELETED
				&& array[ currentPos ].element != theValue
				&& iter < capacity()
			);
			
			return currentPos;
			
		}
		
		
		void rehash(){
			
			// Copy the element 
			std::vector<HashEntry> oldArray = array;
			
			// Clear the original array 
			array.clear();
			
			// Resize the array 
			array.resize( nextPrime( oldArray.size() + 1 ) );
			
			// Rehash the old elements 
			numHash = 0;
			for( HashEntry & entry : oldArray ){
				
				if(entry.state == ACTIVE){
					insert(entry.element);
				}
			}
		}
	
	
	public:
	
		HashTable(const unsigned int size = 0) : numHash(0), array(){
			
			array.resize( nextPrime(size) );
		}
		
		virtual ~HashTable() {}

		bool insert( const Value& theValue ){
			
			// Get the position we'd want to insert 
			unsigned int currentPos = findPos(theValue);
			
			// If active:
			// 1) Found a copy of theValue 
			// 2) No more available buckets
			if( isActive(currentPos) ){
				
				return false;
			}
			
			HashEntry theEntry(theValue, ACTIVE);
			++numHash;
			
			array[ currentPos ] = theEntry;
			
			if( numHash > capacity() / 2 ){
				rehash();
			}
			
			return true;
		}

		bool remove( const Value& theValue ){
			
			unsigned int currentPos = findPos( theValue );
			
			// If it's not active - Not in the Hash 
			if( !isActive( currentPos ) ){
				return false;
			}
			
			// Lazy Deletion
			array[ currentPos ].state = DELETED;
			
			--numHash;
			
			return true;
		}

		friend std::ostream& operator<<(std::ostream& output, const HashTable<Key,Value>& hash){
			
			output << "# of Hashed Elements: " << hash.numHash << " ";
			output << "Hash Capacity: " << hash.array.capacity() << std::endl;
			
			for(unsigned int iter = 0 ; iter < hash.capacity(); ++iter){
				
				output << "{"<< iter << ", ";
				
				if(hash.array[ iter ].state == ACTIVE){
					output << "ACTIVE, " << hash.array[iter].element;
				}
				else if(hash.array[ iter ].state == EMPTY){
					output << "EMPTY, ";
				}
				else{
					output << "DELETED, " << hash.array[iter].element;
				}
				
				output << "}";
				
				std::cout << std::endl;
			}
			
			return output;
		}
	
};

#endif