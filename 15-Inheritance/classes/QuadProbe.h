#ifndef QUADPROBE_H
#define QUADPROBE_H

#include "LinearProbe.h"

const unsigned int C1 = 1;
const unsigned int C2 = 1;


template<class Key, class Value>
class QuadProbe : public HashTable<Key, Value>{
	
	
	private:
	
		unsigned int findPos(Value theValue) const{
			
			unsigned int iter = 0;
			unsigned int currentPos = HashFunc(theValue);
			
			do{
				currentPos = (currentPos + C1*iter + C2*iter*iter) % (unsigned int)this->capacity();
				++iter;
			}
			while(
				this->array[ currentPos ].state != EMPTY
				&& this->array[ currentPos ].state != DELETED
				&& this->array[ currentPos ].element != theValue
				&& iter < this->capacity()
			);
			
			return currentPos;
			
		}
	
	
	
	public:
	
		QuadProbe(const unsigned int size = 0) : HashTable<Key, Value>(size) {}
		
		
		~QuadProbe() {}
		

		friend std::ostream& operator<<(std::ostream& output, const QuadProbe<Key,Value>& hash){
			
			output << "# of Hashed Elements: " << hash.numHash << " ";
			output << "Hash Capacity: " << hash.array.capacity() << std::endl;
			
			for(unsigned int iter = 0 ; iter < hash.capacity(); ++iter){
				
				output << "{"<< iter << ", ";
				
				if(hash.array[ iter ].state == ACTIVE){
					output << "ACTIVE, " << hash.array[iter].element;
				}
				else if(hash.array[ iter ].state == EMPTY){
					output << "EMPTY, ";
				}
				else{
					output << "DELETED, " << hash.array[iter].element;
				}
				
				output << "}";
				
				std::cout << std::endl;
			}
			
			return output;
		}
	
};


#endif